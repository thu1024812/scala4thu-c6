package apps

/**
//  * Created by mark on 16/04/2017.
//  */
//object HelloWorld extends App{
//  val somebody= readLine()
//  println("Hello "+somebody+"!!")
//
//}


object HelloWorld extends App{
  println("Hello World!!")
}

object HelloWorld1 extends App{
  val somebody= "Ernestine"
  println("Hello "+somebody+"!!")

}

object HelloWorld2 extends App{
  val somebody= "Ernestine"
  println(s"Hello $somebody !!")
}

//不用回傳用foreach
object HelloWorld3 extends App{
  args.foreach(somebody=>{
    println(s"Hello $somebody !!")
  })
}
//類型轉換用map  args.map()

object HelloWorld5 extends App{
  val somebody1= args(1)
  println(s"Hello $somebody1 !!")
}

object HelloWorld4 extends App{
  print("Enter your name: ")
  val somebody= readLine()
  println(s"Hello $somebody !!")
}
//It's HW2!!

//打包：用terminal:先打sbt package  再打scala -cp target/scala-2.10/scala4thu-c6_2.10-1.0.jar apps.HelloWorld ＃此為路徑
//可用scala target/  再打跑出的路徑   來找路徑


